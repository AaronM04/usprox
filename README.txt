Extremely basic Unix socket proxy. All it does is print out the client and server communications with color coding.

Usage:

  ./usprox.py --original=<originalsocket> --new=<proxiedsocket>

  Connections to <proxiedsocket> will be forwarded to <originalsocket>

Example:

  ./usprox.py --original=/var/www/run/php-fpm.sock --new=/var/www/run/proxied-php-fpm.sock
