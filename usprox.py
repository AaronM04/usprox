#!/usr/bin/env python3

from socket import socket, AF_UNIX, SOCK_STREAM
import threading
import sys
import os
from queue import SimpleQueue

from optparse import OptionParser


# From https://stackoverflow.com/a/287944
class bcolors:
    HEADER    = '\033[95m'
    OKBLUE    = '\033[94m'
    OKCYAN    = '\033[96m'
    OKGREEN   = '\033[92m'
    WARNING   = '\033[93m'
    FAIL      = '\033[91m'
    ENDC      = '\033[0m'
    BOLD      = '\033[1m'
    UNDERLINE = '\033[4m'


class event:
    FROM_CLIENT = 0
    TO_CLIENT   = 1
    CONNECT     = 2
    CLOSE       = 3
    PROXY_EXIT  = 4
    EXC         = 5


def queue_printer(queue):
    "Thread for reading tuples from queue and printing them"
    while True:
        client_id, evt, msg = queue.get()
        if evt == event.PROXY_EXIT:
            print("PROXY_EXIT")
            return
        if isinstance(msg, bytes):
            msg = msg.decode('utf-8', errors='replace')
        msg = msg.replace('\r', '')
        for line in filter(lambda line: line != '', msg.split('\n')):
            if evt == event.FROM_CLIENT:
                print(f"{bcolors.OKBLUE}[{client_id}] {line}{bcolors.ENDC}")
            elif evt == event.TO_CLIENT:
                print(f"{bcolors.BOLD}[{client_id}] {line}{bcolors.ENDC}")
            elif evt == event.CONNECT:
                print(f"{bcolors.OKGREEN}[{client_id}] {line}{bcolors.ENDC}")
            elif evt == event.EXC:
                print(f"{bcolors.FAIL}[{client_id}] EXC: {line}{bcolors.ENDC}")
            else: # close
                print(f"{bcolors.WARNING}[{client_id}] {line}{bcolors.ENDC}")
        print()
        print()


def read_write_tapper(queue, client_id, evt, read_sock, write_sock):
    "Thread to read from read_sock, write to write_sock, and copy msg to queue."
    direction = "from_client"
    if evt == event.TO_CLIENT:
        direction = "to_client"
    try:
        while True:
            msg = read_sock.recv(8192)
            if len(msg) == 0:
                break
            queue.put((client_id, evt, msg))
            write_sock.sendall(msg)
    except ConnectionResetError:
        pass
    except Exception as e:
        queue.put((client_id, event.EXC, f"Exception {e} in {direction} direction"))
    finally:
        write_sock.close()
        queue.put((client_id, event.CLOSE, f"Closed {direction} direction"))


def connect_to_svr(originalf):
    "Note: if this blocks, it blocks accepting connections"
    sock = socket(AF_UNIX, SOCK_STREAM)
    sock.connect(originalf)
    return sock


def main():
    parser = OptionParser()
    parser.add_option('--original', dest='originalf', help='Socket file to fwd connections to')
    parser.add_option('--new', dest='newf', help='Socket file to listen on')
    (options, _) = parser.parse_args()
    originalf, newf = (options.originalf, options.newf)
    if originalf is None or newf is None:
        parser.print_help()
        sys.exit(1)

    lsock = socket(AF_UNIX, SOCK_STREAM)
    lsock.bind(newf)
    lsock.listen(1)

    queue = SimpleQueue()
    threading.Thread(target=queue_printer, args=(queue,)).start()

    try:
        client_id = 1
        while True:
            s_from_client, _ = lsock.accept()
            queue.put((client_id, event.CONNECT, "Connected :)"))
            s_to_client = s_from_client.dup() # duplicate because socket isn't thread-safe

            s_from_server = connect_to_svr(originalf)
            s_to_server = s_from_server.dup()

            # Start a thread for each direction of connection.
            threading.Thread(target=read_write_tapper, args=(queue, client_id, event.FROM_CLIENT, s_from_client, s_to_server)).start()
            threading.Thread(target=read_write_tapper, args=(queue, client_id, event.TO_CLIENT, s_from_server, s_to_client)).start()

            client_id += 1
    except KeyboardInterrupt:
        queue.put((None, event.PROXY_EXIT, None))
        lsock.close()
    finally:
        os.unlink(newf)

if __name__ == '__main__':
    main()
